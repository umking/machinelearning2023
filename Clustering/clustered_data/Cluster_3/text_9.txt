Aktivasyon fonksiyonları
 Relu 
   avantajı hızlı ve doğrusal olmayan farklılaştırılabilir yapısıyla kullanışlıdır kaybolan gradient sorunu yoktur büyük sinir ağlarının gizli katmanlarında çok kullanışlıdır
   dezavantaj ReLU katmanının en büyük dezavantajı Dying Neurons probleminden muzdarip olmasıdır Girdiler negatif olduğunda türevi sıfır olur bu nedenle geri yayılma gerçekleştirilemez ve bu nöron için öğrenme gerçekleşmeyebilir ve ölür
foo  tfconstant     dtype  tffloat
tfkerasactivationsrelufoonumpy
tfkerasactivationsrelufoo alphanumpy
tfkerasactivationsrelufoo max_valuenumpy
tfkerasactivationsrelufoo thresholdnumpy
 Sigmoid 
   avantaj Çıkışı  ile  arasında değiştiği için ikili sınıflandırma için iyi bir seçimdir
   dezavantaj Giriş değerleri çok küçük veya çok yüksekse sinir ağı öğrenmeyi durdurabilir bu sorun popüler olarak yok olan gradyan problemi olarak bilinir Bu nedenle Sigmoid etkinleştirme işlevi gizli katmanlarda kullanılmaz
sigmoidx     expx
 Softmax Softmax işlevi değer aralığı  ile toplam e eşit olan bir vektör olarak bir olasılık dağılımı üretir
   avantaj Softmax bir olasılık dağılımı ürettiğinden çok sınıflı sınıflandırma için bir çıktı katmanı olarak kullanılır
 Tanh 
   avantaj Çıkışı  ila  arasında değiştiği için bir nöronun çıkışının negatif olmasını istediğimizde kullanılabilir
   dezavantaj Çalışması bir sigmoid fonksiyona benzediğinden çok düşük veya çok yüksek giriş değerleri varsa vanishing gradyanı sorunundan da muzdariptir
Hangi nöral ağ kullanılmalı

Yok Olan Degrade probleminden muzdarip oldukları için gizli katmanlarda Sigmoid ve Tanh aktivasyon fonksiyonundan kaçınılmalıdır
İkili Sınıflandırma durumunda çıkış katmanında sigmoid aktivasyon fonksiyonu kullanılmalıdır
ReLU aktivasyon fonksiyonları Yok Olan Degrade probleminden muzdarip olmadıkları ve hesaplamalı olarak hızlı oldukları için sinir ağlarının gizli katmanları için idealdir
Çok katlı sınıflandırma durumunda çıktı katmanında Softmax aktivasyon fonksiyonu kullanılmalıdır
Wed Dec    GMT GMT
httpsmachinelearningknowledgeaikerasactivationlayersultimateguideforbeginners
