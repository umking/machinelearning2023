import requests
from bs4 import BeautifulSoup
import googletrans
from googletrans import Translator

url = "https://www.example.com"
response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')

links = []
for link in soup.find_all('a'):
    href = link.get('href')
    if href is not None:
        links.append(href)

translator = Translator()
for i in range(len(links)):
    translation = translator.translate(links[i], src='en', dest='hi')
    links[i] = translation.text

with open('links.txt', 'w', encoding='utf-8') as file:
    for link in links:
        file.write(link + '\n')
Thu Mar 02 2023 14:39:56 GMT+0300 (GMT+03:00)
https://chat.openai.com/chat